# Space Invaders

A web-based space invaders game made purely in JavaScript.

![alt text](img/snapshot.png "Snapshot of the game")

*It's a bit dull now, but it'll improve"*

## About

This project uses the [HTML5 Boilerplate](https://html5boilerplate.com/), [Gulp](http://gulpjs.com/) for automated tasks and [Jasmine](http://jasmine.github.io/) for JavaScript tests. You can run the site locally at localhost:4000 using [Browsersync](https://www.browsersync.io/). Check [their guide](https://www.browsersync.io/docs/gulp/) out.

To handle key pressing across browsers easier, the [jQuery Hotkeys plugin](https://github.com/tzuryby/jquery.hotkeys) is used. It also includes a 16-line JS wrapper that will make event querying available, by Daniel X. Moore.

## Install npm dependencies

```bash
$ npm install
```

## Run tasks

```bash
$ gulp
$ gulp watch
```

## Run tests

Open the `dev/js/SpecHelper.html` file in a browser.

## Run the site

```bash
$ gulp server
```

and open `localhost:4000` in a browser.

## License

[![License](https://img.shields.io/badge/gnu-license-green.svg?style=flat)](https://opensource.org/licenses/GPL-2.0)
GNU License
