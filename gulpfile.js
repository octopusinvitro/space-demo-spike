var autoprefixer = require('gulp-autoprefixer'),
    browsersync  = require('browser-sync').create(),
    concat       = require('gulp-concat'),
    gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    uglify       = require('gulp-uglify'),
    paths        = {
  cssdev:  './cssdev/main.scss',
  cssdist: './css/',
  jsdev:   [
    './jsdev/src/jquery-hotkeys.js',
    './jsdev/src/key-status.js',
    './jsdev/src/util.js',
    './jsdev/src/sprite.js',
    './jsdev/src/sound.js',
    './jsdev/src/game.js',
    './jsdev/plugins.js',
    './jsdev/main.js'
  ],
  jsdist:  './js/',
};

gulp.task('sass', function() {
  return gulp
    .src(paths.cssdev)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.cssdist));
});

gulp.task('js', function() {
  return gulp
    .src(paths.jsdev)
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.jsdist));
});

gulp.task('default', ['sass', 'js']);

gulp.task('watch', function() {
  gulp.watch('./cssdev/*', ['sass']);
  gulp.watch(paths.jsdev,  ['js']);
  gulp.watch('./cssdev/*', browsersync.reload);
  gulp.watch(paths.jsdev,  browsersync.reload);
});

gulp.task('server', function() {
  browsersync.init({
    server: {
      baseDir: './',
    },
    port:   4000,
    notify: false,
    open:   false
  });
});
